import 'dart:convert';

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pickle/controllers/loading.dart';
import 'package:pickle/controllers/navigation.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/models/topics.dart';
import 'package:pickle/utils/constant.dart';
import 'package:pickle/widgets/header.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pickle/widgets/notif.dart';
import 'package:pickle/widgets/settings.dart';

class CodeDetails extends GetView<LoadingController> {
  final Topics topic;

  CodeDetails(this.topic);
  @override
  Widget build(BuildContext context) {

    NavigationController navC = Get.find<NavigationController>();
    ThemeController themeC = Get.find<ThemeController>();

    String firstelement = "";
    String secondelement = null;
    String thirdelement = null;
    String image = "assets/panel.jpeg";
    switch(topic.type){
      case 1 :
        firstelement = "Code ";
        // secondelement = topic.explanation;
        break;
      case 2 :
        firstelement = topic.name;
        secondelement = topic.immatr + " - " + topic.station;
        image = "assets/taxi.png";
        thirdelement = topic.phone;
        break;
      case 3 :
        firstelement = topic.name;
        secondelement = topic.immatr + " - Line " + topic.line;
        image = "assets/bus.jpg";
        thirdelement = topic.phone;
        break;
      case 4 :
        firstelement = topic.name;
        secondelement = topic.immatr + " - " + topic.station;
        image = "assets/transport.jpg";
        thirdelement = topic.phone;
        break;
      case 5 :
        firstelement = topic.name;
        secondelement = topic.address;
        image = "assets/driving.jpg";
        thirdelement = topic.phone;
        break;
      case 6 :
        firstelement = topic.name;
        secondelement = topic.address;
        image = "assets/garage.jpg";
        thirdelement = topic.phone;
        break;
      case 7 :
        firstelement = topic.product;
        secondelement = topic.ref;
        image = "assets/store.jpg";
        thirdelement = topic.price.toString() + " Ar";
        break;
      case 8 :
        firstelement = topic.name;
        secondelement = topic.address;
        image = "assets/assurance.jpg";
        thirdelement = topic.phone;
        break;
    }
    List<Widget> _widgets = [
      Column(
        children: [
          Header(showBackButton: true,),
          Container(
            height: 569.h,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: 200.h,
                        width: 200.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(image: topic.picto != null ?MemoryImage(base64Decode(topic.picto.split(',')[1])) : AssetImage(image))
                        ),

                      ),
                    ],
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(child: Text(firstelement,style: TextStyle(fontWeight: FontWeight.w700,fontSize: 22.ssp),))
                      ],
                    ),
                  ),
                  secondelement != null ? Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(child: Text(secondelement,style: TextStyle(fontSize: 18.ssp,height: 1.8),))
                          ],
                        )

                      ],
                    ),
                  ) : SizedBox(width: 0,height: 0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 2.h,
                        margin: EdgeInsets.symmetric(vertical: 20),
                        color: Colors.yellow,
                        width: 300.w,
                      ),
                    ],
                  ),
                  thirdelement != null ? Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(child: Text(thirdelement,style: TextStyle(fontSize: 18.ssp),))
                          ],
                        )

                      ],
                    ),
                  ) : SizedBox(width: 0,height: 0,),
                  thirdelement != null ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 2.h,
                        margin: EdgeInsets.symmetric(vertical: 20),
                        color: Colors.yellow,
                        width: 300.w,
                      ),
                    ],
                  ) : SizedBox(width: 0,height: 0,),
                  topic.explanation != null ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 300.w,
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.symmetric(vertical: 12.h),
                                child: Text('Explication',style: TextStyle(fontWeight: FontWeight.w700,fontSize: 22.ssp))),
                            Container(
                                child: Text(topic.explanation,
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(fontSize: 18.ssp,height: 2.h)
                                )
                            )
                          ],
                        ),
                      ),
                    ],
                  ) : SizedBox(width: 0,height: 0,),
                  topic.explanation != null ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 2,
                        margin: EdgeInsets.symmetric(vertical: 20.h),
                        color: Colors.yellow,
                        width: 300.w,
                      ),
                    ],
                  ) : SizedBox(width: 0,height: 0,),
                ],
              ),
            ),
          )
          // Container(width: Get.width,height: 200,de)
        ],
      ),
      Notif(),
      Settings()
    ];

    return  Scaffold(
      backgroundColor: Colors.transparent,
      body: Obx( () {
          return Container(
            height: 1.sh,
            width: 1.sw,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: themeC.darkMode.value  ? <Color>[
                  Color(0xffe2d891),
                  Color(0xFFBEBEBE)]
                    :  <Color>[
                  Color(0xfff6efb7),
                  Color(0xFFFFFAf6)
                ],
              ),
            ),
            child: Obx((){
              return _widgets[navC.currentIndex.value];
            })
          );
        }
      ),
      bottomNavigationBar:  Material(
        child: Obx(() {
          return Container(
            height: 56.h,
            child: BottomNavigationBar(
              currentIndex: navC.currentIndex.value,
              selectedItemColor: Constant.basecolor,
              onTap: (index){
                if(index == 1){
                  controller.nofisNumber.value = 0 ;
                  GetStorage().write("pikla_notification",0);
                }
                navC.currentIndex.value  = index;
              },
              selectedFontSize: 13.ssp,
              unselectedFontSize: 13.ssp,// this will be set when a new tab is tapped
              items: [
                BottomNavigationBarItem(

                  icon: new Icon(Icons.home,size: 20.w,),
                  label: 'Accueil',
                ),
                BottomNavigationBarItem(
                  icon: controller.nofisNumber.value != 0 ? Badge(badgeContent: Text(controller.nofisNumber.value.toString()),child: Icon(Icons.notifications,size: 20.w),) : new Icon(Icons.notifications,size: 20.w),
                  label: 'Notifications',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.settings,size: 20.w),
                  label: 'Parametres',

                )
              ],
            ),
          );
        }
        ),
      ),
    );
  }
  
}