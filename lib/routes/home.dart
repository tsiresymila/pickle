import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pickle/controllers/loading.dart';
import 'package:pickle/controllers/navigation.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/utils/constant.dart';
import 'file:///D:/Project/Android/pickle/lib/widgets/notif.dart';
import 'package:pickle/widgets/card.dart';
import 'package:pickle/widgets/header.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pickle/widgets/search_bar.dart';
import 'package:pickle/widgets/settings.dart';

class Home extends GetView<LoadingController> {

  final List<Widget> _widgets = [
    Column(
      children: [
        Header(showBackButton: false,),
        SearchBar(),
        Container(
          height: 499.h,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                CardWidget("assets/panel.jpeg",'CODE DE LA ROUTE','Description...',0),
                CardWidget("assets/taxi.png",'TAXI','Description...',1),
                CardWidget("assets/bus.jpg",'TAXI BE','Description...',2),
                CardWidget("assets/transport.jpg",'TRANSPORT/LOCATION/MOTO','Description...',3),
                CardWidget("assets/driving.jpg",'AUTO ECOLE','Description...',4),
                CardWidget("assets/garage.jpg",'GARAGE/LAVAGE/ASSISTANCE','Description...',5),
                CardWidget("assets/store.jpg",'BOUTIQUE','Description...',6),
                CardWidget("assets/assurance.jpg",'ASSURANCE','Description...',7),
              ],
            ),
          ),
        )
        // Container(width: Get.width,height: 200,de)
      ],
    ),
    Notif(),
    Settings()
  ];

  Widget build(BuildContext context) {

    NavigationController navC = Get.find<NavigationController>();
    ThemeController themeC = Get.find<ThemeController>();


    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: false,
      body: Obx(() {
          return Container(
            height: 1.sh,
            width: 1.sw,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: themeC.darkMode.value  ? <Color>[
                  Color(0xffe2d891),
                Color(0xFFBEBEBE)]
                :  <Color>[
                  Color(0xfff6efb7),
                  Color(0xFFFFFAf6)
                ],
              ),
            ),
            child: _widgets[navC.currentIndex.value]
          );
        }
      ),
      bottomNavigationBar: Material(
        child: Obx(() {
            return Container(
              height: 56.h,
              child: BottomNavigationBar(
                currentIndex: navC.currentIndex.value,
                selectedItemColor: Constant.basecolor,
                onTap: (index){
                  if(index == 1){
                    controller.nofisNumber.value = 0 ;
                    GetStorage().write("pikla_notification",0);
                  }
                  navC.currentIndex.value  = index;
                },
                selectedFontSize: 13.ssp,
                unselectedFontSize: 13.ssp,// this will be set when a new tab is tapped
                items: [
                  BottomNavigationBarItem(
                    icon: new Icon(Icons.home,size: 20.w,),
                    label: 'Accueil',
                  ),
                  BottomNavigationBarItem(
                    icon: controller.nofisNumber.value != 0 ? Badge(badgeContent: Text(controller.nofisNumber.value.toString()),child: Icon(Icons.notifications,size: 20.w),) : new Icon(Icons.notifications,size: 20.w),
                    label: 'Notifications',
                  ),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.settings,size: 20.w),
                      label: 'Parametres',

                  )
                ],
              ),
            );
          }
        ),
      ),
    );
  }

}



