import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:pickle/controllers/filter.dart';
import 'package:pickle/controllers/loading.dart';
import 'package:pickle/controllers/navigation.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/utils/constant.dart';
import 'package:pickle/widgets/code_card.dart';
import 'package:pickle/widgets/header.dart';
import 'package:pickle/widgets/notif.dart';
import 'package:pickle/widgets/search_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pickle/widgets/settings.dart';

class RouteCode extends GetView<LoadingController> {

  final int type ;

  RouteCode(this.type) ;
  @override
  Widget build(BuildContext context) {

    NavigationController navC = Get.find<NavigationController>();
    ThemeController themeC = Get.find<ThemeController>();

    List<Widget> _widgets = [
      Column(
        children: [
          Header(showBackButton: true,),
          // Container(
          //   margin: EdgeInsets.symmetric(horizontal: 24.w),
          //   height: 50.h,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     mainAxisSize: MainAxisSize.max,
          //     children: [
          //       Flexible(
          //           flex : 2,
          //           child: GetBuilder<FilterController>(
          //               builder: (fcontroller) {
          //                 return InkWell(
          //                   child: Obx(() =>(
          //                       Container(
          //                         margin: EdgeInsets.only(bottom: 12.h),
          //                         alignment: Alignment.center,
          //                         height: 30.h,
          //                         decoration: BoxDecoration(
          //                             color: fcontroller.active.value =="all" ? Colors.grey : Color(0xfff5f5f5),
          //                             borderRadius: BorderRadius.only(bottomLeft: Radius.elliptical(20, 20),topRight: Radius.elliptical(20, 20)),
          //                             boxShadow: [
          //                               BoxShadow(color: Color(0xfff1f1f1),blurRadius: 1,offset: Offset(-2,-2)),
          //                               BoxShadow(color: Color(0xffacacac),blurRadius: 1,offset: Offset(2,2))
          //                             ]
          //                         ),
          //                         child: Text("Tout",style: TextStyle(color: fcontroller.active.value =="all" ? Colors.white70 : Colors.black26),),
          //
          //                       )
          //                   ),
          //                   ),
          //                   onTap: (){
          //                     fcontroller.active.value = "all";
          //                   },
          //                 );
          //               }
          //           )
          //       ),
          //       Flexible(
          //           flex : 5,
          //           child: GetBuilder<FilterController>(
          //               builder: (fcontroller) {
          //                 return InkWell(
          //                   child: Obx(()=>(
          //                       Container(
          //                         margin: EdgeInsets.only(left: 12.w,bottom: 12.h),
          //                         alignment: Alignment.center,
          //                         height: 30.h,
          //                         decoration: BoxDecoration(
          //                             color: fcontroller.active.value == "mark" ? Colors.grey : Color(0xfff5f5f5),
          //                             borderRadius: BorderRadius.only(bottomLeft: Radius.elliptical(20, 20),topRight: Radius.elliptical(20, 20)),
          //                             boxShadow: [
          //                               BoxShadow(color: Color(0xfff1f1f1),blurRadius: 1,offset: Offset(-2,-2)),
          //                               BoxShadow(color: Color(0xffacacac),blurRadius: 1,offset: Offset(2,2))
          //                             ]
          //
          //                         ),
          //                         child: Text("Marquage au Sol",style: TextStyle(color: fcontroller.active.value =="mark" ? Colors.white70 : Colors.black26),),
          //                       )
          //                   )),
          //                   onTap: (){
          //                     debugPrint("Hello");
          //                     fcontroller.active.value = "mark";
          //                   },
          //                 );
          //               }
          //           )),
          //       Flexible(
          //           flex : 3,
          //           child: GetBuilder<FilterController>(
          //               builder: (fcontroller) {
          //                 return InkWell(
          //                   child: Obx(()=>(
          //                       Container(
          //                           alignment: Alignment.center,
          //                           margin: EdgeInsets.only(left: 12.w,bottom: 12.h),
          //                           height: 30.h,
          //                           decoration: BoxDecoration(
          //                               color: fcontroller.active.value =="pannel" ? Colors.grey : Color(0xfff5f5f5),
          //                               borderRadius: BorderRadius.only(bottomLeft: Radius.elliptical(20, 20),topRight: Radius.elliptical(20, 20)),
          //                               boxShadow: [
          //                                 BoxShadow(color: Color(0xfff1f1f1),blurRadius: 1,offset: Offset(-2,-2)),
          //                                 BoxShadow(color: Color(0xffacacac),blurRadius: 1,offset: Offset(2,2))
          //                               ]
          //
          //                           ),
          //                           child: Text("Panneau",style: TextStyle(color: fcontroller.active.value =="pannel" ? Colors.white70 : Colors.black26),)
          //
          //                       )
          //                   ),
          //                   ),
          //                   onTap: (){
          //                     fcontroller.active.value = "pannel";
          //                   },
          //                 );
          //               }
          //           ))
          //     ],
          //   ),
          // ),
          // SearchBar(),
          Container(
            height: 549.h,
            child: LiquidPullToRefresh(
              onRefresh: controller.pullRefresh,
              showChildOpacityTransition: false,
              color: Constant.basecolor,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                physics: AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: controller.topics[this.type].map((e) => CodeCard(e)).toList()
                ),
              ),
            ),
          )
          // Container(width: Get.width,height: 200,de)
        ],
      ),
      Notif(),
      Settings()
    ];
      return Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: false,
        body: Obx((){
            return Container(
              height: 1.sh,
              width: 1.sw,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: themeC.darkMode.value  ? <Color>[
                    Color(0xffe2d891),
                    Color(0xFFBEBEBE)]
                      :  <Color>[
                    Color(0xfff6efb7),
                    Color(0xFFFFFAf6)
                  ],
                ),
              ),
              child: Obx((){
                return _widgets[navC.currentIndex.value];
              })
            );
          }
        ),
        bottomNavigationBar:  Material(
          child: Obx(() {
            return Container(
              height: 56.h,
              child: BottomNavigationBar(
                currentIndex: navC.currentIndex.value,
                selectedItemColor: Constant.basecolor,
                onTap: (index){
                  if(index == 1){
                    controller.nofisNumber.value = 0 ;
                    GetStorage().write("pikla_notification",0);
                  }
                  navC.currentIndex.value  = index;
                },
                selectedFontSize: 13.ssp,
                unselectedFontSize: 13.ssp,// this will be set when a new tab is tapped
                items: [
                  BottomNavigationBarItem(

                    icon: new Icon(Icons.home,size: 20.w,),
                    label: 'Accueil',
                  ),
                  BottomNavigationBarItem(
                    icon:   controller.nofisNumber.value != 0 ? Badge(badgeContent: Text(controller.nofisNumber.value.toString()),child: Icon(Icons.notifications,size: 20.w),) : new Icon(Icons.notifications,size: 20.w),
                    label: 'Notifications',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.settings,size: 20.w),
                    label: 'Parametres',

                  )
                ],
              ),
            );
          }
          ),
        ),
      );
  }

}

