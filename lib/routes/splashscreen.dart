import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:pickle/controllers/loading.dart';
import 'package:pickle/utils/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class SplashScreen extends GetView<LoadingController> {
  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.transparent,
        body: Container(
          height: 1.sh,
          width: 1.sw,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: <Color>[
                Color(0xfff6efb7),
                Color(0xFFFFFAf6)
              ],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
                Container(
                  child: Image.asset("assets/logo.png",width: 250.w,),
                ),
                Obx(()=> SpinKitThreeBounce(color: controller.isSync.value ? Constant.secondColor :Constant.basecolor)),
                Container(
                  child: Obx(()=> Text(controller.textlabel.value,style: TextStyle(fontSize: 16.ssp,fontWeight: FontWeight.w600,color: Color(0xffacacac)),)),
                )
            ],
          ),
        ),
    );
  }

}