import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:pickle/models/notification.dart';
import 'package:pickle/models/topics.dart';
import 'package:pickle/routes/home.dart';
import 'package:pickle/utils/connection.dart';
import 'package:pickle/utils/database.dart';
import 'package:connectivity/connectivity.dart';


class LoadingController extends GetxController {

  RxDouble currentProgress = 0.0.obs;
  RxInt indication = 0.obs;
  RxString textlabel = "Chargement . . .".obs;
  setLabel (newlabel) => textlabel = newlabel;
  RxBool isSync = false.obs;
  String get label => textlabel.value;

  Timer timer;
  RxInt nofisNumber = 0.obs ;
  List<List<Topics>> topics = [];
  List<Topics> topics1 = [];
  List<Topics> topics2 = [];
  List<Topics> topics3 = [];
  List<Topics> topics4 = [];
  List<Topics> topics5 = [];
  List<Topics> topics6 = [];
  List<Topics> topics7 = [];
  List<Topics> topics8 = [];
  List<Notifs> notifications = [];
  DatabaseHelper database ;
  Connection connection;
  var box = GetStorage();

  @override
  void onInit() async{
    connection = new Connection();
    database = DatabaseHelper();
    await loadingDatabase().then((value) => Get.offAll(Home()));

    super.onInit();
  }

  Future<void> loadingDatabase()async{

      await database.open();
      textlabel.value = "Connexion . . ." ;
      int version = box.read("pikla_version") ?? 0 ;
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
        Map<String, dynamic> data = await connection.getDatabase();
        topics = [];
        topics1 = [];
        topics2 = [];
        topics3 = [];
        topics4 = [];
        topics5 = [];
        topics6 = [];
        topics7 = [];
        topics8 = [];
        notifications = [];
        if(data != null){

          List databaseData = data['database']['topics'];
          List databaseNotifsData = data['notification']['topics'];
          textlabel.value = "Synchronisation . . ." ;
          await database.truncate();
          for(var element in databaseData) {
            Map <String,dynamic> el = Map.from(element);
            Topics topic = new Topics();
            topic.fromMap(el);
            switch(topic.type){
              case 1 :
                topics1.add(topic);
                break;
              case 2 :
                topics2.add(topic);
                break;
              case 3 :
                topics3.add(topic);
                break;
              case 4 :
                topics4.add(topic);
                break;
              case 5 :
                topics5.add(topic);
                break;
              case 6 :
                topics6.add(topic);
                break;
              case 7 :
                topics7.add(topic);
                break;
              case 8 :
                topics8.add(topic);
                break;
            }
            await database.insert(topic);
          }
          topics =  [topics1,topics2,topics3,topics4,topics5,topics6,topics7,topics8];
          //Notifs datatabase
          database.truncatenotifs();
          for(var element in databaseNotifsData){
            Map <String,dynamic> el = Map.from(element);
            Notifs notif = new Notifs();
            notif.fromMap(el);
            database.insertNotif(notif);
            notifications.add(notif);
          }
          box.write("pikla_notification",databaseNotifsData.length);
          nofisNumber.value = databaseNotifsData.length ;
          isSync.value = true;
        }
        else{
          print('Erreur de connection');
          textlabel.value = "Synchronisation . . ." ;
          isSync.value = true;
          topics = await database.queryAllRows();
          notifications = await database.queryAllNotifsRows();
        }
      } else {
          textlabel.value = "Synchronisation . . ." ;
          isSync.value = true;
          topics = await database.queryAllRows();
          notifications = await database.queryAllNotifsRows();
      }
  }

  Future<List<Topics>> searchTopics(String pattern)async{
    await database.open();
    return await database.findTopics(pattern);
  }
  Future<void> pullRefresh()async{
    await loadingDatabase();
  }

  @override
  Future onClose() {
    super.onClose();
    timer.cancel();
    return null;
  }

}