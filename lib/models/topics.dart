class Topics  {
  int id;
  int type;
  String picto;
  String explanation;
  String name ;
  String vehicule;
  String immatr;
  String phone;
  String station;
  String line;
  String address;
  String product;
  String ref ;
  int price;
  String street;
  int  version ;
  int  soft_delete;

  Topics();

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'type': type,
      'picto':picto,
      'explanation':explanation,
      'name' : name,
      'vehicule':vehicule,
      'immatr':immatr,
      'phone' : phone,
      'station' : station,
      'line' : line,
      'address':address,
      'product' :product,
      'ref' : ref,
      'price' : price,
      'street' : street,
      'version' : version,
      'soft_delete' : soft_delete
    };
    if (id != null) {
      map['_id'] = id;
    }
    return map;
  }


  fromMap(Map<String, dynamic> map) {
    this.id = map['_id'];
    this.type = int.parse(map['type'].toString());
    this.picto = map['picto'];
    this.explanation = map['explanation'];
    this.name = map['name'];
    this.vehicule = map['vehicule'];
    this.immatr = map['immatr'];
    this.phone = map['phone'];
    this.station = map['station'];
    this.line = map['line'];
    this.address = map['address'];
    this.product = map['product'];
    this.ref = map['ref'];
    this.price = map['price'] == null ? 0 : int.parse(map['price'].toString());
    this.street = map['street'];
    this.version = map['version'];
    this.soft_delete = map['soft_delete'];

  }
}