class Notifs  {
  int id;
  String title;
  String description;
  String image ;
  String created;

  Notifs();

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'title':title,
      'description':description,
      'image' : image,
      'created':created,
    };
    if (id != null) {
      map['_id'] = id;
    }
    return map;
  }


  fromMap(Map<String, dynamic> map) {
    this.id = map['_id'];
    this.title = map['title'];
    this.description = map['description'];
    this.image = map['image'];
    this.created = map['created'];
  }
}