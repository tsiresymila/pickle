import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pickle/controllers/filter.dart';
import 'package:pickle/controllers/navigation.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/routes/SplashScreen.dart';
import 'package:pickle/utils/constant.dart';
import 'controllers/loading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() async{
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Get.put(LoadingController(), permanent: true);
    Get.put(FilterController(),permanent: true);
    Get.put(NavigationController(),permanent: true);
    Get.put(ThemeController(),permanent: true);
    debugPrint(Get.width.toString()+' '+Get.height.toString());
    return ScreenUtilInit(
            designSize: Size(392, 825),
            allowFontScaling: false,
            builder: () =>
                GetMaterialApp(
                     title: 'Pickle',
                     debugShowCheckedModeBanner: false,
                     theme: Constant.light,
                     darkTheme: Constant.dark,
                     home: SplashScreen(),
                   )
            );
  }
}
