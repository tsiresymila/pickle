import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Constant {
  static Color basecolor = Color(0xFFFFEB00);
  static Color secondColor = Color(0xFFFFF693);
  static const MaterialColor maincolor = MaterialColor(
    _primaryColor,
    <int, Color>{
      50: Color(0xFFFCE4EC),
      100: Color(0xFFF8BBD0),
      200: Color(0xFFF48FB1),
      300: Color(0xFFF06292),
      400: Color(0xFFEC407A),
      500: Color(_primaryColor),
      600: Color(0xFFD81B60),
      700: Color(0xFFC2185B),
      800: Color(0xFFAD1457),
      900: Color(0xFF880E4F),
    },
  );
  static const int _primaryColor = 0xFFFFEB00;
  static final light = ThemeData.light().copyWith(
    backgroundColor: Colors.white,
    buttonColor: basecolor,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    primaryColor: basecolor
  );
  static final dark = ThemeData.dark().copyWith(
    backgroundColor: Colors.grey,
    buttonColor: basecolor,
    primaryColor: basecolor,
    bottomNavigationBarTheme: BottomNavigationBarThemeData(backgroundColor: Colors.grey,selectedLabelStyle: TextStyle(color: basecolor)),
    visualDensity: VisualDensity.adaptivePlatformDensity,
  );

}