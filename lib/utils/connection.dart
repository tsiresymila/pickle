import 'dart:convert';
import 'package:get/get.dart';
import 'package:dio/dio.dart' as dio;

typedef  ErrorCallBack = Function(String error) ;
typedef SuccessCallback = Function(Map<String, dynamic> data);

class Connection extends GetConnect {

  String url = "/topics/list" ;
  String notifsUrl = "/notifications/list";
  String urlVersion = "https://jsonplaceholder.typicode.com/todos/1";
  dio.BaseOptions options = new dio.BaseOptions(
      baseUrl:
      'https://admin.pikla.mg',
      // connectTimeout: 180000,
      // receiveTimeout: 180000
  );
  Future<Map<String, dynamic>> getDatabase() async{
    print('connection ...');
    dio.Dio req = new dio.Dio(options);
    try{
      dio.Response response = await req.get(url);
      dio.Response notifications = await req.get(notifsUrl);
      Map<String, dynamic> data = {
        "notification": notifications.data,
        "database":  response.data,
      };
      return data;
    }catch(e){
      print('connection error ...'+e.toString());
      return null ;
    }

  }

  getVersion(){
    get(urlVersion).then((Response version)async{
      Map<String, dynamic> data = await this.getDatabase();
      data['version'] = version.bodyString;
      });
  }
}