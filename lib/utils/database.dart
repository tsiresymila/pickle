import 'package:pickle/models/notification.dart';
import 'package:pickle/models/topics.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  String table = "pikla_topics";
  String ntable = "pikla_notifs";
  Database db;

  String columnId = '_id';
  String columnType = 'type';
  String columnImage = 'picto';
  String columnExplanation = 'explanation';
  String columnName = 'name';
  String columnVehicule = 'vehicule';
  String columnImmatr = 'immatr';
  String columnPhone = 'phone';
  String columnStation = 'station';
  String columnLine = 'line';
  String columnAddress = 'address';
  String columnProduct = 'product';
  String columnRef = 'ref';
  String columnPrice = 'price';
  String columnStreet = 'street';
  String columnMigration = 'version';
  String columnSoftDelete = 'soft_delete';

  String ncolumnId = '_id';
  String ncolumnTitle = 'title';
  String ncolumnImage = 'image';
  String ncolumnDescription = 'description';
  String ncolumnDate = 'created';
  String ncolumnSoftDelete = 'soft_delete';

  Future open() async {
    String databasesPath = (await getDatabasesPath());
    String path = join(databasesPath, 'pikla.db');
    db = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
      await db.execute(
          ''' create table if not exists $table ( 
                    $columnId integer primary key autoincrement, 
                    $columnType integer,
                    $columnImage text,
                    $columnExplanation text,
                    $columnName varchar(255),
                    $columnVehicule varchar(255),
                    $columnImmatr varchar(255),
                    $columnPhone varchar(255),
                    $columnStation varchar(255),
                    $columnLine varchar(255),
                    $columnAddress  varchar(255),
                    $columnProduct  varchar(255),
                    $columnRef  varchar(255),
                    $columnPrice  varchar(255),
                    $columnStreet  varchar(255),
                    $columnMigration  integer,
                    $columnSoftDelete  integer );
          '''
      );
      await db.execute(
          ''' create table if not exists $ntable ( 
                    $ncolumnId integer primary key autoincrement, 
                    $ncolumnTitle varchar(255),
                    $ncolumnImage text,
                    $ncolumnDescription text,
                    $ncolumnDate varchar(255),
                    $ncolumnSoftDelete  integer );
          '''
      );
    });
  }

  Future<Topics> insert(Topics topic) async {
    topic.id = await db.insert(table, topic.toMap());
    return topic;
  }

  Future<Notifs> insertNotif(Notifs notif) async {
    notif.id = await db.insert(ntable, notif.toMap());
    return notif;
  }

  Future<List<List<Topics>>> queryAllRows() async {
    List<Map> results =  await db.query(table);
    List<List<Topics>> topics = [];
    List<Topics> topics1 = [];
    List<Topics> topics2 = [];
    List<Topics> topics3 = [];
    List<Topics> topics4 = [];
    List<Topics> topics5 = [];
    List<Topics> topics6 = [];
    List<Topics> topics7 = [];
    List<Topics> topics8 = [];
    results.forEach((result) {
      Topics topic = new Topics();
      Map <String,dynamic> el = Map.from(result);
      topic.fromMap(el);
      switch(topic.type){
        case 1 :
          topics1.add(topic);
          break;
        case 2 :
          topics2.add(topic);
          break;
        case 3 :
          topics3.add(topic);
          break;
        case 4 :
          topics4.add(topic);
          break;
        case 5 :
          topics5.add(topic);
          break;
        case 6 :
          topics6.add(topic);
          break;
        case 7 :
          topics7.add(topic);
          break;
        case 8 :
          topics8.add(topic);
          break;
      }
    });
    return [topics1,topics2,topics3,topics4,topics5,topics6,topics7,topics8];
  }

  Future<List<Notifs>> queryAllNotifsRows() async {
    List<Map> results =  await db.query(ntable);
    List<Notifs> notifs = [];

    results.forEach((result) {
      Notifs notif = new Notifs();
      Map <String,dynamic> el = Map.from(result);
      notif.fromMap(el);
      notifs.add(notif);

    });
    return notifs;
  }

  Future<Topics> getTopic(int id) async {
    List<Map<String,dynamic>> maps = await db.query(table,
        columns: [columnId, columnType, columnImage,columnExplanation,columnName,columnVehicule,columnVehicule,
          columnImmatr,columnPhone,columnStation,columnLine,columnAddress,columnProduct,columnRef,columnPrice,
          columnAddress,columnMigration,columnSoftDelete],
        where: '$columnId = ? and $columnSoftDelete = ?',
        whereArgs: [id,0]);
    if (maps.length > 0) {
      Topics topic = Topics();
      topic.fromMap(maps.first);
      return topic;
    }
    return null;
  }

  Future<List<Topics>> findTopics(String pattern) async {
    List<Topics> topics = [];
    List<Map<String,dynamic>> results = await db.query(table,
        columns: [columnId, columnType, columnImage,columnExplanation,columnName,columnVehicule,
          columnImmatr,columnPhone,columnStation,columnLine,columnProduct,columnRef,columnPrice,
          columnAddress,columnMigration,columnSoftDelete],
        where: "$columnMigration like '%$pattern%'  or $columnAddress like '%$pattern%'  or $columnName like '%$pattern%'  or $columnExplanation like '%$pattern%' or $columnPhone like '%$pattern%'  or $columnVehicule like '%$pattern%' or $columnImmatr like '%$pattern%'  or $columnStation like '%$pattern%'  or $columnLine like '%$pattern%'  or $columnProduct like '%$pattern%'  or $columnRef like '%$pattern%'");
    if (results.length > 0) {
      for(var result in results){
        Topics topic = new Topics();
        Map <String,dynamic> el = Map.from(result);
        topic.fromMap(el);
        topics.add(topic);
      }

    }
    return topics;
  }

  Future<Notifs> getNotifs(int id) async {
    List<Map<String,dynamic>> maps = await db.query(ntable,
        columns: [ncolumnId, ncolumnTitle, ncolumnImage,ncolumnDescription,ncolumnDate,ncolumnSoftDelete],
        where: '$ncolumnId = ? and $ncolumnSoftDelete = ?',
        whereArgs: [id,0]);
    if (maps.length > 0) {
      Notifs notif = new  Notifs();
      notif.fromMap(maps.first);
      return notif;
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }
  Future<int> deleteNotifs(int id) async {
    return await db.delete(ntable, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(Topics topic) async {
    return await db.update(table, topic.toMap(),
        where: '$columnId = ?', whereArgs: [topic.id]);
  }
  Future<int> updateNotifs(Notifs notif) async {
    return await db.update(ntable, notif.toMap(),
        where: '$columnId = ?', whereArgs: [notif.id]);
  }

  Future truncate() async {
    return await db.execute("DELETE FROM " + table);
}
  Future truncatenotifs() async {
    return await db.execute("DELETE FROM " + ntable);
  }

  Future close() async => db.close();

}
