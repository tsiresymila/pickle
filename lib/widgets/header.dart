import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/utils/constant.dart';
import 'package:pickle/widgets/roundedclipper.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Header extends StatelessWidget {
  final bool showBackButton ;
  const Header({ this.showBackButton,
  });

  @override
  Widget build(BuildContext context) {
    ThemeController themeC = Get.find<ThemeController>();
    return Obx(() {
        return Container(
          height: 200.h,
          child: Stack(
            alignment: AlignmentDirectional.topCenter,
            children: [
              ClipPath(
                clipper: RoundedClipper2(200.h),
                child: Container(
                  height: 200.h,
                  width: 1.sw,
                  decoration: BoxDecoration(
                      color:Constant.secondColor
                  ),
                ),
              ),
              ClipPath(
                clipper: RoundedClipper1(190.h),
                child: Container(
                  height: 200.h,
                  width: 1.sw,
                  decoration: BoxDecoration(
                      color:Constant.basecolor
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset('assets/entete1.png',width: 160.w,height: 100.h,),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset('assets/logo.png',width: 160.w,height: 100.h,),
                    )
                  ],
                ),),
              this.showBackButton? Container(
                height: 220.h,
                alignment: Alignment.bottomLeft,
                child: Row(
                  children: [
                    InkWell(
                      onTap: (){
                        Get.back();
                      },
                      child: Container(
                        padding: EdgeInsets.all(4),
                        margin: EdgeInsets.only(left: 8.w,bottom: 8.h),
                        decoration: BoxDecoration(
                          color:  themeC.darkMode.value ? Colors.grey : Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(color: themeC.darkMode.value ? Color(0xffa8a8a8) : Color(0xfff1f1f1),blurRadius: 1,offset: Offset(-1,-1)),
                            BoxShadow(color: themeC.darkMode.value ? Color(0xffa8a8a8) : Color(0xffacacac),blurRadius: 1,offset: Offset(1,1))
                          ],
                          border: Border.all(color:themeC.darkMode.value ? Color(0xffa8a8a8) :  Color(0xfff1f1f1))
                        ),
                          child: Icon(Icons.subdirectory_arrow_left)),
                    )
                  ],
                ),
              ): themeC.darkMode.value ? Container(width: 0,height: 0,) : Container(width: 0,height: 0,),

            ],
          ),
        );
      }
    );
  }
}