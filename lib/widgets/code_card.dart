import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/models/topics.dart';
import 'package:pickle/routes/code_details.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CodeCard extends StatelessWidget {
  final Topics topic ;
   CodeCard(this.topic);

  @override
  Widget build(BuildContext context) {

    ThemeController themeC = Get.find<ThemeController>();

    String firstelement = "";
    String secondelement = null;
    String image = "assets/panel.jpeg";
    switch(topic.type){
      case 1 :
        firstelement = "Code ";
        secondelement = topic.explanation.substring(0,min(20, topic.explanation.length));
        break;
      case 2 :
        firstelement = topic.name;
        secondelement = topic.immatr + " - " + topic.station;
        image = "assets/taxi.png";
        break;
      case 3 :
        firstelement = topic.name;
        secondelement = topic.immatr + " - Line " + topic.line;
        image = "assets/bus.jpg";
        break;
      case 4 :
        firstelement = topic.name;
        secondelement = topic.immatr + " - " + topic.station;
        image = "assets/transport.jpg";
        break;
      case 5 :
        firstelement = topic.name;
        secondelement = topic.address;
        image = "assets/driving.jpg";
        break;
      case 6 :
        firstelement = topic.name;
        secondelement = topic.address;
        image = "assets/garage.jpg";
        break;
      case 7 :
        firstelement = topic.product;
        secondelement = topic.ref;
        image = "assets/store.jpg";
        break;
      case 8 :
        firstelement = topic.name;
        secondelement = topic.address;
        image = "assets/assurance.jpg";
        break;
    }

    return Obx((){
        return Container(
          margin:EdgeInsets.symmetric(horizontal: 20.w,vertical: 8.h),
          decoration:BoxDecoration(
              border: Border.all(color: themeC.darkMode.value ?  Colors.grey : Colors.white70),
              color:  themeC.darkMode.value ? Colors.grey :  Colors.white ,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(color: themeC.darkMode.value ? Color(0xffa8a8a8) : Color(0xfff1f1f1),blurRadius: 1,offset: Offset(-1,-1)),
                BoxShadow(color: themeC.darkMode.value ? Color(0xffa8a8a8) : Color(0xffacacac),blurRadius: 1,offset: Offset(1,1))
              ]
          ),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 6.w),
            leading: topic.picto != null ?Image.memory(base64Decode(topic.picto.split(',')[1])) : Image.asset(image),
            title: Text(firstelement,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 18.ssp),),
            subtitle: Text(secondelement),
            trailing: InkWell(
              onTap: (){
                Get.to(CodeDetails(topic));
              },
              child: Container(
                child:Icon(Icons.arrow_forward,color: Colors.white70,),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.yellow
              ),),
            ),
            onTap: (){
              Get.to(CodeDetails(topic));
            },
          ),
        );
      }
    );
  }
}