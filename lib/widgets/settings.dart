import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/utils/constant.dart';
import 'package:pickle/widgets/header.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Settings extends GetView<ThemeController> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Header(
        showBackButton: false,
      ),
      Container(
        height: 569.h,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Obx((){
            return Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.h),
                decoration: BoxDecoration(
                    border: Border.all(color:  controller.darkMode.value?  Colors.grey: Colors.white70),
                    color: controller.darkMode.value? Colors.grey : Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: controller.darkMode.value?  Color(0xffa8a8a8) : Color(0xfff1f1f1),
                          blurRadius: 1,
                          offset: Offset(-1, -1)),
                      BoxShadow(
                          color:  controller.darkMode.value? Color(0xff8a8a8a) : Color(0xffacacac),
                          blurRadius: 1,
                          offset: Offset(1, 1))
                    ]),
                child: ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 6.w),
                  leading: Icon(
                    Icons.nights_stay,
                    color: Colors.black26,
                    size: 40,
                  ),
                  title: Text(
                    "Mode nuit",
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 18.ssp),
                  ),
                  trailing:  Switch(
                        activeColor: Get.isDarkMode ? Constant.basecolor : Constant.basecolor,
                        onChanged: (state){
                          controller.darkMode.value = state;
                          if(state == true){
                              Get.changeTheme(Constant.dark);
                          }
                          else{
                            Get.changeTheme(Constant.light);
                          }
                        }, value: controller.darkMode.value,
                      )

                  ),
                ),

            ],
          );
          }
          ),
        ),
      )
    ]);
  }
}
