import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RoundedClipper1 extends CustomClipper<Path> {
  final double height;

  RoundedClipper1(this.height);

  @override
  Path getClip(Size size) {
    var path = Path();
      path.lineTo(0.0, 150.h);
    path.quadraticBezierTo(
      size.width / 2,
      height,
      size.width,
      130.h,
    );
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class RoundedClipper2 extends CustomClipper<Path> {
  final double height;

  RoundedClipper2(this.height);

  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, 172.h);
    path.quadraticBezierTo(
      size.width / 2,
      height,
      size.width,
      130.h,
    );
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}