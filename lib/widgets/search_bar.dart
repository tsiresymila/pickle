import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pickle/controllers/loading.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/models/topics.dart';
import 'package:pickle/routes/code_details.dart';

class SearchBar extends GetView<LoadingController> {

   SearchBar();

  @override
  Widget build(BuildContext context) {

    ThemeController themeC = Get.find<ThemeController>();

    return Obx(() {
        return Container(
          height: 50.h,
          alignment: Alignment.center,
          margin: EdgeInsets.only(bottom: 20.h,left: 20.w,right: 20.w,top: 0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              boxShadow: [
                BoxShadow(color: themeC.darkMode.value ?  Color(0xff787878) : Color(0xffe9e9e9),blurRadius: 1,offset: Offset(-1,-1)),
                BoxShadow(color: themeC.darkMode.value ?  Color(0xff787878) : Color(0xffacacac),blurRadius: 1,offset: Offset(1,1))
              ]
          ),
          child: TypeAheadFormField(
            textFieldConfiguration: TextFieldConfiguration(
                keyboardType: TextInputType.text,
                // controller: this._typeAheadController,
                decoration: InputDecoration(
                    fillColor: themeC.darkMode.value ?  Color(0xff787878) : Colors.white,
                    focusColor: themeC.darkMode.value ?  Color(0xff787878) : Colors.white,
                    isDense: true,
                    filled: true,
                    prefixIcon: Icon(Icons.search,color: Colors.grey,),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide:  BorderSide(color: themeC.darkMode.value ?  Color(0xff787878) : Colors.white ),
                        gapPadding: 1
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                        borderSide:  BorderSide(color: themeC.darkMode.value ?  Color(0xff787878) : Colors.white ),
                        gapPadding: 1
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50),
                      borderSide:  BorderSide(color: themeC.darkMode.value ?  Color(0xff787878) : Colors.white ),
                    ),
                    hintText: 'Recherche global',
                    hintStyle: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600)
                )
            ),
            suggestionsCallback: (pattern) async{
              // return CitiesService.getSuggestions(pattern);
              if(pattern == null || pattern == ""){
                return [];
              }
              return await controller.searchTopics(pattern);
            },
            itemBuilder: (context, topico) {
              Topics topic = topico ;
              String firstelement = "";
              String secondelement = null;
              String image = "assets/panel.jpeg";
              switch(topic.type){
                case 1 :
                  firstelement = "Code ";
                  secondelement = topic.explanation.substring(0,math.min(20, topic.explanation.length));
                  break;
                case 2 :
                  firstelement = topic.name;
                  secondelement = topic.immatr + " - " + topic.station;
                  image = "assets/taxi.png";
                  break;
                case 3 :
                  firstelement = topic.name;
                  secondelement = topic.immatr + " - Line " + topic.line;
                  image = "assets/bus.jpg";
                  break;
                case 4 :
                  firstelement = topic.name;
                  secondelement = topic.immatr + " - " + topic.station;
                  image = "assets/transport.jpg";
                  break;
                case 5 :
                  firstelement = topic.name;
                  secondelement = topic.address;
                  image = "assets/driving.jpg";
                  break;
                case 6 :
                  firstelement = topic.name;
                  secondelement = topic.address;
                  image = "assets/garage.jpg";
                  break;
                case 7 :
                  firstelement = topic.product;
                  secondelement = topic.ref;
                  image = "assets/store.jpg";
                  break;
                case 8 :
                  firstelement = topic.name;
                  secondelement = topic.address;
                  image = "assets/assurance.jpg";
                  break;
              }
              return ListTile(
                leading: Image.asset(image),
                title: Text(firstelement),
                subtitle: Text(secondelement),
                onTap: (){
                  Get.to(CodeDetails(topic));
                },
              );

            },
            transitionBuilder: (context, suggestionsBox, controller) {
              return suggestionsBox;
            },
            hideOnEmpty: true,
            onSuggestionSelected: (suggestion) {
              // this._typeAheadController.text = suggestion;
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Please select a city';
              }
              else{
                return '';
              }
            },
            onSaved: (value){
            },
          ),
        );
      }
    );
  }
}