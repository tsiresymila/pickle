import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:pickle/controllers/loading.dart';
import 'package:pickle/utils/constant.dart';
import 'package:pickle/widgets/header.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pickle/widgets/notif_widget.dart';
class Notif extends GetView<LoadingController>{
  @override
  Widget build(BuildContext context) {
    print( controller.notifications);
    return Column(
        children: [
          Header(showBackButton: false,),
          Container(
            height: 569.h,
            child: LiquidPullToRefresh(
              onRefresh: controller.pullRefresh,
              showChildOpacityTransition: false,
              color: Constant.basecolor,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: controller.notifications.map((notif) =>  NotifWidget(notif)).toList()
                ),
              ),
            ),
          )
        ]
    );
  }

}