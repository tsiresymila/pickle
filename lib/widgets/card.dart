
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/routes/code.dart';
import 'package:pickle/utils/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CardWidget extends StatelessWidget {
  final String image;
  final String title;
  final String description;
  final int type ;
  CardWidget(this.image, this.title, this.description, this.type);
  @override
  Widget build(BuildContext context) {

    ThemeController themeC = Get.find<ThemeController>();

    return Obx(() {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 20.w,vertical: 20.h),
          decoration: BoxDecoration(
            color: themeC.darkMode.value ? Colors.grey :Color(0xfff5f5f5),
              border: Border.all(color: themeC.darkMode.value ? Colors.grey :Colors.white),
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(color: themeC.darkMode.value ?  Color(0xff787878) : Color(0xffafafaf), blurRadius: 1.0, spreadRadius: 0),
              ]
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                width: 120.w,height: 110.h,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage(this.image,) ),
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin:EdgeInsets.symmetric(vertical: 3.h,horizontal: 6.w),
                          child: Text(this.title,style: TextStyle(fontWeight: FontWeight.w600))),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 3.h,horizontal: 6.w),
                          child: Text(this.description,style: TextStyle(fontWeight: FontWeight.w600)))
                    ],
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(left:20.w,right: 20.w,bottom: 8.h),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: RaisedButton(
                    color: Constant.basecolor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Constant.basecolor)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Voir plus',style: TextStyle(fontWeight: FontWeight.w600),),
                        Icon(Icons.arrow_right_alt,)
                      ],
                    ),
                    onPressed: (){
                      Get.to(RouteCode(type));
                    },
                  ),
                ),
              )
            ],
          ),
        );
      }
    );
  }
}