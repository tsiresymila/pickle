import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pickle/controllers/theme.dart';
import 'package:pickle/models/notification.dart';
import 'package:pickle/routes/code_details.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pickle/utils/constant.dart';

class NotifWidget extends StatelessWidget {
  final Notifs notifs;
  NotifWidget(this.notifs);

  @override
  Widget build(BuildContext context) {

    ThemeController themeC = Get.find<ThemeController>();

    return Container(
      margin:EdgeInsets.symmetric(horizontal: 20.w,vertical: 8.h),
      decoration:BoxDecoration(
          color: themeC.darkMode.value ? Colors.grey :Color(0xfff5f5f5),
          border: Border.all(color: themeC.darkMode.value ? Colors.grey :Colors.white),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: themeC.darkMode.value ?  Color(0xff787878) : Color(0xfff1f1f1),blurRadius: 2,offset: Offset(-2,-2)),
            BoxShadow(color: themeC.darkMode.value ?  Color(0xff787878) : Color(0xffacacac),blurRadius: 2,offset: Offset(2,2)),

          ]
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 6.w),
            leading: Icon(Icons.notifications,color: Constant.basecolor, size: 40,),
            title: Text(this.notifs.title,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 18.ssp),),
            subtitle: Text(DateFormat("dd-MM-yyyy hh:mm").format(DateTime.parse(this.notifs.created))),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Text(this.notifs.description ,style: TextStyle(height: 1.8),),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child:  this.notifs.image != "" ? Image.memory(base64Decode(this.notifs.image.split(',')[1]),) : SizedBox(width: 0,height: 0,),
            ),
          )
        ],
      ),
    );
  }
}